package nl.codefoundry

data class Address(
    val street: String,
    val houseNumber: String,
    val city: String,
    val country: String
)